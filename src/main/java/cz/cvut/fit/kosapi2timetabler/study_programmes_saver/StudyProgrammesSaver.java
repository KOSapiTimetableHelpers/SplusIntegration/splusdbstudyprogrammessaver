/*
 * To change this license header, choose License Headers mandatory Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template mandatory the editor.
 */
package cz.cvut.fit.kosapi2timetabler.study_programmes_saver;

import cz.cvut.fit.kosapi2timetabler.timetabler_db.Module;
import cz.cvut.fit.kosapi2timetabler.timetabler_db.ProgrammeOfStudy;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author guthondr
 */
public class StudyProgrammesSaver {

    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("TimetablerPG");
    private static final EntityManager ENTITY_MANAGER = ENTITY_MANAGER_FACTORY.createEntityManager();
    private static final Path DATA_FILE_MANDATORY = Paths.get(System.getProperty("user.home"), "/work/timetable/splusStudyPlansB172.csv");
    private static final Path DATA_FILE_OPTIONAL = Paths.get(System.getProperty("user.home"), "/work/timetable/timetablerDbStudyProgrammesOptional.csv");

    public static void main(String[] args) throws IOException {
        try {
            final EntityTransaction et = ENTITY_MANAGER.getTransaction();
           et.begin();
           loadFromFile();
           et.commit();

//               saveToFile();
        } finally {
            ENTITY_MANAGER.close();
            ENTITY_MANAGER_FACTORY.close();
        }
    }

    private static void stream2db(final BufferedReader inReader,
            final Function<ProgrammeOfStudy, Collection<Module>> prog2modsFunction) throws IOException {
        for (String line = inReader.readLine(); line != null; line = inReader.readLine()) {
            final String[] lineArr = line.split(";");
            ProgrammeOfStudy prog = ENTITY_MANAGER.find(ProgrammeOfStudy.class, lineArr[0]);
            if (prog == null) {
                prog = new ProgrammeOfStudy();
                prog.setName(lineArr[0]);
            }
            final TypedQuery<Module> qm = ENTITY_MANAGER.createQuery(
                    "SELECT m FROM Module m WHERE m.code = :code",
                    Module.class);
            qm.setParameter("code", lineArr[1]);
            final List<Module> ql = qm.getResultList();
            if (ql.size() > 0) {
                final Module module = ql.get(0);

                prog2modsFunction.apply(prog).add(module);
                
                ENTITY_MANAGER.merge(prog);
            }
        }
    }

    private static void loadFromFile() throws IOException {
        try (final BufferedReader mandatory = Files.newBufferedReader(DATA_FILE_MANDATORY);
                final BufferedReader optional = Files.newBufferedReader(DATA_FILE_OPTIONAL)) {
            stream2db(mandatory, p -> {
                Collection<Module> m = p.getMandatoryModules();
                if (m == null) {
                    m = new ArrayList<>();
                    p.setMandatoryModules(m);
                }
                return m;
            });
            stream2db(optional, p -> {
                Collection<Module> m = p.getOptionalModules();
                if (m == null) {
                    m = new ArrayList<>();
                    p.setOptionalModules(m);
                }
                return m;
            });
        }
    }

    private static void saveToFile() throws IOException {
        try (final BufferedWriter mandatory = Files.newBufferedWriter(DATA_FILE_MANDATORY);
                final BufferedWriter optional = Files.newBufferedWriter(DATA_FILE_OPTIONAL)) {
            final TypedQuery<ProgrammeOfStudy> pq
                    = ENTITY_MANAGER.createQuery(
                            "SELECT p FROM ProgrammeOfStudy p",
                            ProgrammeOfStudy.class);
            for (final ProgrammeOfStudy p : pq.getResultList()) {
                for (final Module m : p.getMandatoryModules()) {
                    mandatory.write(p.getName() + ";" + m.getCode());
                    mandatory.newLine();
                }
                for (final Module m : p.getOptionalModules()) {
                    optional.write(p.getName() + ";" + m.getCode());
                    optional.newLine();
                }
            }
        }

    }
}
